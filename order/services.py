from django.contrib.auth import get_user_model

from rest_framework.exceptions import NotFound, ValidationError

from order.models import OrderCart, Order
from cart.models import Cart, CartProduct
from cart.services import CartService

User = get_user_model()


class OrderCartService:

    model = OrderCart

    @classmethod
    def create_order_cart(cls, customer: User) -> OrderCart:
        order_cart = cls.model.objects.create(customer=customer)

        return order_cart

    @staticmethod
    def refresh_order_cart_to_zero(order_cart: OrderCart) -> OrderCart:
        order_cart.total_orders = 0
        order_cart.save()

        return order_cart

    @staticmethod
    def refresh_order_cart_with_plus_one_order(order_cart: OrderCart) -> OrderCart:
        order_cart.total_orders += 1
        order_cart.save()

        return order_cart

    @staticmethod
    def refresh_order_cart_with_minus_one_order(order_cart: OrderCart) -> OrderCart:
        order_cart.total_orders -= 1
        order_cart.save()

        return order_cart

    @staticmethod
    def get_all_orders_by_order_cart(order_cart: OrderCart) -> list:

        all_orders = order_cart.order_set.select_related('cart')

        return all_orders


class OrderService:
    model = Order

    @classmethod
    def create_order_and_refresh_to_zero_order_cart_and_cart(cls, cart: Cart, order_cart: OrderCart) -> Order:
        new_order = cls.model.objects.create(cart=cart, order_cart=order_cart)

        OrderCartService.refresh_order_cart_with_plus_one_order(order_cart=order_cart)
        CartService.refresh_cart_to_zero(cart=cart)
        CartProduct.objects.filter(cart=cart).delete()

        return new_order

    @classmethod
    def get_or_create_order(cls, cart: Cart, order_cart: OrderCart) -> Order:

        is_exist_order = cls.is_exist_order_by_cart_and_order_cart(cart, order_cart)

        if is_exist_order:
            order = cls.model.objects.get(cart=cart, order_cart=order_cart)
        else:
            order = cls.create_order_and_refresh_to_zero_order_cart_and_cart(cart, order_cart)

        return order

    @classmethod
    def get_or_create_order_or_raise_exception(cls, cart: Cart, order_cart: OrderCart) -> Order:
        is_empty_cart = CartService.is_empty_cart(cart)
        is_exist_order = cls.is_exist_order_by_cart_and_order_cart(cart, order_cart)

        if is_empty_cart and not is_exist_order:
            msg = {'detail': 'Your cart is empty to place an order'}
            raise cls.validation_error(msg)

        elif is_empty_cart and is_exist_order:
            return cls.model.objects.get(cart=cart, order_cart=order_cart)

        elif not is_exist_order and not is_exist_order:
            return cls.create_order_and_refresh_to_zero_order_cart_and_cart(cart, order_cart)

    @staticmethod
    def delete_order(order: Order) -> None:
        order.delete()

    @classmethod
    def is_exist_order_by_cart_and_order_cart(cls, cart: Cart, order_cart: OrderCart, raise_exception=False) -> bool:

        is_exist = True

        try:
            cls.model.objects.get(cart=cart, order_cart=order_cart)
        except cls.model.DoesNotExist as e:
            is_exist = False

        if not is_exist and raise_exception:
            msg = {'detail': 'Not found any orders'}
            raise NotFound(msg)

        return is_exist

    @staticmethod
    def validation_error(msg: dict):
        return ValidationError(msg)
