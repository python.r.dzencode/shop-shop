from django.contrib.auth import get_user_model
from django.db import models

from cart.models import Cart

User = get_user_model()


class OrderCart(models.Model):
    customer = models.OneToOneField(User, on_delete=models.CASCADE, related_name='order_cart')
    total_orders = models.PositiveIntegerField(default=0)

    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'OrderCart for `{self.customer}` customer'


class Order(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    order_cart = models.ForeignKey(OrderCart, on_delete=models.CASCADE)

    total_amount = models.DecimalField(max_digits=16, decimal_places=2, blank=False, default=0.00)  # max value 99999999999999.99
    total_products_count = models.PositiveIntegerField(default=0)

    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        self.total_amount = self.cart.total_amount
        self.total_products_count = self.cart.total_products_count

        super(Order, self).save(*args, **kwargs)

    def __str__(self):
        return f'Order for `{self.cart}` cart'
