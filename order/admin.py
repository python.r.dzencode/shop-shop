from django.contrib import admin

from order.models import OrderCart, Order


@admin.register(OrderCart)
class OrderCartAdmin(admin.ModelAdmin):
    list_display = ('pk', 'customer', 'total_orders', 'updated_at', 'created_at')
    list_display_links = ('pk', 'customer')
    readonly_fields = ('updated_at', 'created_at')


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('pk', 'cart', 'order_cart', 'total_amount', 'total_products_count', 'updated_at', 'created_at')
    list_display_links = ('pk', 'cart')
    readonly_fields = ('updated_at', 'created_at')
