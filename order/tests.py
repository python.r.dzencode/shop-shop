from decimal import Decimal

from django.db import transaction

from rest_framework.test import APITestCase, APIRequestFactory, force_authenticate

from auth_app.services import UserService
from cart.services import CartService, CartProductService
from product.services import ProductService
from order.views import GetOrderView
from order.services import OrderService
from order.serializers import OrderSerializer


class OrderTest(APITestCase):

    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.view = GetOrderView
        self.uri = 'get-order/'
        self.user = self.setup_user(username='test', password='test')
        cart = self.user.cart
        self.cart = self.setup_cart(cart)
        self.order_cart = self.user.order_cart
        self.order = self.setup_order(self.cart, self.order_cart)
        self.order_expected_data = OrderSerializer(instance=self.order).data

    @staticmethod
    def setup_user(username: str, password: str):
        user = UserService.create_new_user(username=username, password=password)

        return user

    @staticmethod
    def setup_order(cart, order_cart):
        return OrderService.get_or_create_order(cart, order_cart)

    def setup_cart(self, cart):
        product1 = ProductService.create_new(self.user, 'Test', Decimal(123.4), 'Test Desc')
        product2 = ProductService.create_new(self.user, 'Test', Decimal(123.4), 'Test Desc')
        product3 = ProductService.create_new(self.user, 'Test', Decimal(123.4), 'Test Desc')

        with transaction.atomic():
            CartProductService.create_cart_product(cart, product1)
            CartService.refresh_cart_with_plus_one_product(cart, product1)
            CartProductService.create_cart_product(cart, product2)
            CartService.refresh_cart_with_plus_one_product(cart, product2)
            CartProductService.create_cart_product(cart, product3)
            CartService.refresh_cart_with_plus_one_product(cart, product3)

            return cart

    def test_1_get_order(self):

        request = self.factory.get(path=self.uri)
        force_authenticate(request, self.user)
        response = self.view.as_view()(request)
        status_code = response.status_code

        self.assertEqual(status_code, 200)

    def test_2_check_response_data(self):

        request = self.factory.get(path=self.uri)
        force_authenticate(request, self.user)
        response = self.view.as_view()(request)
        expected_data = self.order_expected_data
        response_data = response.data

        self.assertEqual(response_data, expected_data)

    def test_3_get_order_without_access(self):

        request = self.factory.get(path=self.uri)
        # force_authenticate(request, self.user)
        response = self.view.as_view()(request)
        status_code = response.status_code

        self.assertEqual(status_code, 401)
