from django.urls import path

from order import views

app_name = 'order'

urlpatterns = [
    path('get-order/', views.GetOrderView.as_view(), name='get_order'),
    path('delete-order/', views.DeleteOrderView.as_view(), name='delete_order'),
]
