from django.db import transaction

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import BasicAuthentication, TokenAuthentication

from order.services import OrderCartService, OrderService
from order.models import Order
from order.serializers import OrderSerializer


class GetOrderView(APIView):
    authentication_classes = (BasicAuthentication, TokenAuthentication, )
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        user = request.user

        cart = user.cart
        order_cart = user.order_cart

        with transaction.atomic():
            order = OrderService.get_or_create_order_or_raise_exception(cart=cart, order_cart=order_cart)

            serializer = OrderSerializer(instance=order)

            return Response(serializer.data)


class DeleteOrderView(APIView):
    authentication_classes = (BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        user = request.user

        cart = user.cart
        order_cart = user.order_cart

        with transaction.atomic():
            order = Order.objects.get(cart=cart, order_cart=order_cart)

            OrderService.delete_order(order)

            OrderCartService.refresh_order_cart_to_zero(order_cart)

            return Response(status=status.HTTP_204_NO_CONTENT)
