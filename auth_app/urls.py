from django.urls import path

from auth_app import views

app_name = 'auth_app'

urlpatterns = [
    path('register/', views.RegisterUserView.as_view(), name='register_user'),

]
