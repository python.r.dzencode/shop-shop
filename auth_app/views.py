from django.contrib.auth import get_user_model

from rest_framework.generics import CreateAPIView

from auth_app.serializers import UserSerializer

User = get_user_model()


class RegisterUserView(CreateAPIView):
    authentication_classes = ()
    permission_classes = ()
    model = User
    serializer_class = UserSerializer
