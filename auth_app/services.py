from django.contrib.auth import get_user_model

from rest_framework.authtoken.models import Token

from cart.services import CartService
from order.services import OrderCartService

User = get_user_model()


class UserService:
    user_model = User
    token_model = Token

    @classmethod
    def create_new_user(cls, username: str, password: str) -> user_model:
        user = cls.user_model.objects.create(username=username)

        user.set_password(password)
        user.save()

        CartService.create_cart(customer=user)
        OrderCartService.create_order_cart(customer=user)

        return user


class TokenAPIService:
    token_model = Token

    @classmethod
    def get_or_create_token_for_user(cls, user) -> Token:
        token = cls.token_model.objects.get_or_create(user=user)

        return token
