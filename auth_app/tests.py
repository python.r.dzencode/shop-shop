from rest_framework.test import APITestCase, APIRequestFactory

from auth_app.views import RegisterUserView
from auth_app.services import UserService, TokenAPIService


class TestAPIUser(APITestCase):

    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.view = RegisterUserView.as_view()
        self.uri = '/api/auth/register/'
        self.user = self.setup_user()

    @staticmethod
    def setup_user():
        user = UserService.create_new_user(username='testuser', password='testuser')
        TokenAPIService.get_or_create_token_for_user(user)
        return user

    def test_1_create_user(self):
        params = {
            'username': 'test',
            'password': 'test',
            'password2': 'test'
        }
        request = self.factory.post(path=self.uri, data=params)
        response = self.view(request)
        status_code = response.status_code
        self.assertEqual(status_code, 201)

    def test_2_create_user_with_wrong_passwords(self):
        params = {
            'username': 'test2',
            'password': 'test2',
            'password2': 'test'
        }
        request = self.factory.post(path=self.uri, data=params)
        response = self.view(request)
        status_code = response.status_code
        self.assertEqual(status_code, 400)

    def test_3_try_create_exist_user(self):
        params = {
            'username': self.user.username,
            'password': 'testuser',
            'password2': 'testuser'
        }
        request = self.factory.post(path=self.uri, data=params)
        response = self.view(request)
        status_code = response.status_code
        self.assertEqual(status_code, 400)

