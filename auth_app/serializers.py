from django.contrib.auth import get_user_model

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from auth_app import services

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):

    password2 = serializers.CharField(max_length=155, write_only=True)

    class Meta:
        model = User
        fields = ('username', 'password', 'password2')
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def to_representation(self, instance):
        data = super(UserSerializer, self).to_representation(instance)

        data['token'] = instance.auth_token.key

        return data

    def create(self, validated_data):
        username = validated_data.get('username')
        password = validated_data.get('password')
        password2 = validated_data.get('password2')

        if password != password2:
            raise ValidationError({'detail': 'Password mismatch'})

        user = services.UserService.create_new_user(username=username, password=password)
        services.TokenAPIService.get_or_create_token_for_user(user=user)

        return user
