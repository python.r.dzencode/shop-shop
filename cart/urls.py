from django.urls import path

from cart import views

app_name = 'cart'

urlpatterns = [
    path('cart-product/add-product/', views.AddProductToCustomerCartView.as_view(), name='add_product_to_cart_product'),
    path('volume/', views.GetCartVolumeView.as_view(), name='get_cart_volume'),
    path('delete-all-products-from-cart/', views.DeleteAllProductsFromCartView.as_view(), name='delete_all_products_from_cart'),
    path('delete-one-product-from-cart/', views.DeleteOneProductFromCartView.as_view(), name='delete_one_product_from_cart'),
]
