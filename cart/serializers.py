from django.db import transaction

from rest_framework import serializers

from cart.models import CartProduct, Cart
from cart.services import CartService, CartProductService
from order.services import OrderService

from product.serializers import ProductSerializer


class CartDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Cart
        fields = '__all__'


class CartProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = CartProduct
        fields = ('product', )

    def to_representation(self, instance):
        serializer = ProductSerializer(instance=instance.product)
        data = serializer.data

        return data

    def create(self, validated_data):
        request = self.context['request']

        cart = request.user.cart
        order_cart = request.user.order_cart
        is_exist_order = OrderService.is_exist_order_by_cart_and_order_cart(cart, order_cart)
        if not is_exist_order:
            product = validated_data.get('product')

            with transaction.atomic():
                new_cart_product = CartProductService.create_cart_product(cart=cart, product=product)

                CartService.refresh_cart_with_plus_one_product(cart, product)

        else:
            msg = {'detail': 'You cannot add a product to your cart, you have an unprocessed order'}
            raise OrderService.validation_error(msg)

        return new_cart_product


class ProductsCartDetailSerializer(serializers.Serializer):

    cart = CartDetailSerializer()
    products = ProductSerializer(many=True)
