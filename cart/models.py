from django.db import models
from django.contrib.auth import get_user_model

from product.models import Product

User = get_user_model()


class Cart(models.Model):

    customer = models.OneToOneField(User, on_delete=models.CASCADE)
    total_products_count = models.PositiveIntegerField(default=0)
    total_amount = models.DecimalField(max_digits=16, decimal_places=2, blank=False, default=0.00)  # max value 99999999999999.99

    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'Cart for {self.customer}'


class CartProduct(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE, related_name='cart_products')
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'CartProduct for {self.cart} {self.product}'
