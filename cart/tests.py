from decimal import Decimal

from django.db import transaction

from rest_framework.test import APITestCase, APIRequestFactory, force_authenticate

from auth_app.services import UserService

from cart.services import CartProductService, CartService
from cart.views import AddProductToCustomerCartView, GetCartVolumeView, DeleteAllProductsFromCartView, DeleteOneProductFromCartView
from cart.serializers import ProductsCartDetailSerializer

from product.services import ProductService
from product.serializers import ProductSerializer


class CartTest(APITestCase):

    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.view = AddProductToCustomerCartView
        self.view_get_cart_volume = GetCartVolumeView
        self.view_delete_all_products_from_cart = DeleteAllProductsFromCartView
        self.view_delete_one_product_from_cart = DeleteOneProductFromCartView
        self.uri = ''
        self.user = self.setup_user(username='testuser', password='testuser')
        self.user2 = self.setup_user(username='testuser2', password='testuser2')
        self.cart = self.user.cart
        self.product1 = ProductService.create_new(self.user, 'Test product name 1', Decimal(100), 'Test product desc.')
        self.product2 = ProductService.create_new(self.user, 'Test product name 2', Decimal(24.3), 'Test product desc.')
        self.product3 = ProductService.create_new(self.user, 'Test product name 3', Decimal(12.6), 'Test product desc.')
        cart_products_data = {'cart': self.cart, 'products': []}
        self.valid_cart_volume_data = ProductsCartDetailSerializer(cart_products_data).data

    @staticmethod
    def setup_user(username, password):
        user = UserService.create_new_user(username, password)
        return user

    def test_1_check_cart_total_amount_and_total_products_count(self):
        """
            Checks the correspondence of the cart fields (cart.total_amount and cart.total_products_count).
            Test passed if:
                self.cart.total_amount == expected data (136.9).
                AND
                self.cart.total_products_count == expected data (3).
        """

        expected_total_amount = float(self.product1.price + self.product2.price + self.product3.price)
        expected_total_products_count = 3

        with transaction.atomic():
            CartProductService.create_cart_product(self.cart, self.product1)
            CartService.refresh_cart_with_plus_one_product(self.cart, self.product1)
            CartProductService.create_cart_product(self.cart, self.product2)
            CartService.refresh_cart_with_plus_one_product(self.cart, self.product2)
            CartProductService.create_cart_product(self.cart, self.product3)
            CartService.refresh_cart_with_plus_one_product(self.cart, self.product3)

            self.assertEqual(float(self.cart.total_amount), expected_total_amount)
            self.assertEqual(self.cart.total_products_count, expected_total_products_count)

    def test_2_check_status_code_cart_volume(self):
        """
            Checks status for request cart volume.
            Test passed is response.status_code == 200.
        """

        url = self.uri + 'volume/'

        request = self.factory.get(path=url)
        force_authenticate(request, self.user)
        response = self.view_get_cart_volume.as_view()(request)
        status_code = response.status_code
        self.assertEqual(status_code, 200)

    def test_3_check_response_data_cart_volume(self):
        """
            Checks the returned data with the expected data. Request to cart volume logic.
            Test passed if response.data == expected data (self.valid_cart_volume_data)
        """

        url = self.uri + 'volume/'

        request = self.factory.get(path=url)
        force_authenticate(request, self.user)
        response = self.view_get_cart_volume.as_view()(request)
        res_data = response.data
        valid_data = self.valid_cart_volume_data
        self.assertEqual(res_data, valid_data)

    def test_4_check_access_cart_volume(self):
        """
            Checks the permission for cart volume for AnonymousUser.
            Test passed id response.status_code == 401.
        """

        url = self.uri + 'volume/'

        request = self.factory.get(path=url)
        # force_authenticate(self.user)
        response = self.view_get_cart_volume.as_view()(request)
        status_code = response.status_code
        self.assertEqual(status_code, 401)

    def test_5_delete_all_products_from_cart(self):

        url = self.uri + 'delete-all-products-from-cart/'

        CartProductService.create_cart_product(self.cart, self.product1)

        request = self.factory.delete(path=url)
        force_authenticate(request, self.user)
        response = self.view_delete_all_products_from_cart.as_view()(request)
        status_code = response.status_code

        self.assertEqual(status_code, 204)

    def test_6_delete_one_product_from_cart(self):

        url = self.uri + 'delete-one-product-from-cart/'

        CartProductService.create_cart_product(self.cart, self.product1)

        params = {'product_pk': self.product1.pk}
        request = self.factory.delete(path=url, data=params)
        force_authenticate(request, self.user)
        response = self.view_delete_one_product_from_cart.as_view()(request)
        status_code = response.status_code

        self.assertEqual(status_code, 204)


class CartProductTest(APITestCase):

    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.view = AddProductToCustomerCartView
        self.uri = 'cart-product/'
        self.user = self.setup_user(username='testuser', password='testuser')
        self.user2 = self.setup_user(username='testuser2', password='testuser2')
        self.cart = self.user.cart
        self.product = ProductService.create_new(self.user, 'Test product name', Decimal(124.3), 'Test product desc.')
        self.cart_product = self.setup_cart_product(self.cart, self.product)
        self.valid_serializer_product_data = ProductSerializer(instance=self.product).data

    @staticmethod
    def setup_user(username, password):
        user = UserService.create_new_user(username, password)
        return user

    @staticmethod
    def setup_cart_product(cart, product):
        with transaction.atomic():
            new_cart_product = CartProductService.create_cart_product(
                cart=cart, product=product
            )

            return new_cart_product

    def test_1_create_cart_product(self):
        """
            Creates valid cart product.
            Test passed if response.status_code == 201.
        """

        url = self.uri + 'add-product/'
        params = {
            'product': self.product.pk
        }

        request = self.factory.post(path=url, data=params)
        force_authenticate(request, self.user)
        response = self.view.as_view()(request)
        status_code = response.status_code
        self.assertEqual(status_code, 201)

    def test_2_create_cart_product_with_bad_pk(self):
        """
            Trying to creates not valid cart product. Product is not found.
            Test passed if response.status_code == 400.
        """

        url = self.uri + 'add-product/'
        params = {
            'product': 9999999
        }

        request = self.factory.post(path=url, data=params)
        force_authenticate(request, self.user)
        response = self.view.as_view()(request)
        status_code = response.status_code
        self.assertEqual(status_code, 400)

    def test_3_create_cart_product_without_auth(self):
        """
            Trying creates valid cart product as AnonymousUser.
            Test passed if response.status_code == 401.
        """

        url = self.uri + 'add-product/'
        params = {
            'product': self.product.pk
        }

        request = self.factory.post(path=url, data=params)
        # force_authenticate(request, self.user)
        response = self.view.as_view()(request)
        status_code = response.status_code
        self.assertEqual(status_code, 401)

    def test_4_create_cart_product_get_method(self):
        """
            Trying to request with GET method.
            Test passed if response.status_code == 405.
        """

        url = self.uri + 'add-product/'
        params = {
            'product': self.product.pk
        }

        request = self.factory.get(path=url, data=params)
        force_authenticate(request, self.user)
        response = self.view.as_view()(request)
        status_code = response.status_code
        self.assertEqual(status_code, 405)

    def test_5_create_cart_product_and_check_response_data(self):
        """
            Creates and receives valid cart product object and tries to compare the output data with the expected data.
            Test passed if response.data == expected data.
        """

        url = self.uri + 'add-product/'
        params = {
            'product': self.product.pk
        }

        request = self.factory.post(path=url, data=params)
        force_authenticate(request, self.user)
        response = self.view.as_view()(request)
        res_data = response.data
        valid_data = self.valid_serializer_product_data
        status_code = response.status_code
        self.assertEqual(status_code, 201)
        self.assertEqual(res_data, valid_data)
