from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator
from django.conf import settings
from django.db import transaction

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView
from rest_framework.authentication import BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from product.models import Product

from cart.services import CartService
from cart.models import CartProduct
from cart.permissions import IsCartOwnerAndIsAuth
from cart.serializers import CartProductSerializer, ProductsCartDetailSerializer


cache_timeout = settings.DEFAULT_CACHE_TIMEOUT


class AddProductToCustomerCartView(CreateAPIView):
    authentication_classes = (BasicAuthentication, TokenAuthentication)
    permission_classes = (IsCartOwnerAndIsAuth, )
    serializer_class = CartProductSerializer

    def create(self, request, *args, **kwargs):
        with transaction.atomic():
            return super(AddProductToCustomerCartView, self).create(request, *args, **kwargs)


@method_decorator(cache_page(cache_timeout), name='get')
class GetCartVolumeView(APIView):
    authentication_classes = (BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated, )

    def get(self, request, *args, **kwargs):
        user = request.user

        cart = user.cart
        all_products = CartService.get_all_products_by_cart(cart=cart)

        serializer_data = {'cart': cart, 'products': all_products}
        serializer = ProductsCartDetailSerializer(serializer_data)

        return Response(serializer.data)


class DeleteAllProductsFromCartView(APIView):
    authentication_classes = (BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated, )

    def delete(self, request, *args, **kwargs):
        user = request.user
        cart = user.cart
        with transaction.atomic():

            all_cart_products = CartProduct.objects.filter(cart=cart)
            all_cart_products.delete()

            CartService.refresh_cart_to_zero(cart)

            return Response(status=status.HTTP_204_NO_CONTENT)


class DeleteOneProductFromCartView(APIView):
    authentication_classes = (BasicAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated, )

    def delete(self, request, *args, **kwargs):
        user = request.user
        pk = request.data.get('product_pk')

        with transaction.atomic():
            cart = user.cart
            product = Product.objects.get(pk=pk)

            CartProduct.objects.filter(product=product, cart=cart).delete()
            CartService.refresh_cart_with_minus_one_product(cart, product)

            return Response(status=status.HTTP_204_NO_CONTENT)
