from decimal import Decimal

from django.contrib.auth import get_user_model

from product.models import Product
from cart.models import Cart, CartProduct

User = get_user_model()


class CartService:
    model = Cart

    @classmethod
    def create_cart(cls, customer: User) -> Cart:
        cart = cls.model.objects.create(customer=customer)

        return cart

    @staticmethod
    def refresh_cart_to_zero(cart: Cart) -> Cart:
        cart.total_products_count = 0
        cart.total_amount = 0.0
        cart.save()

        return cart

    @staticmethod
    def refresh_cart_with_plus_one_product(cart: Cart, product: Product) -> Cart:
        cart.total_products_count += 1
        total_amount = Decimal(cart.total_amount)
        total_amount += product.price
        cart.total_amount = total_amount
        cart.save()

        return cart

    @staticmethod
    def refresh_cart_with_minus_one_product(cart, product) -> Cart:
        if cart.total_products_count > 0:
            cart.total_products_count -= 1

        total_amount = Decimal(cart.total_amount)
        total_amount -= product.price
        cart.total_amount = total_amount
        cart.save()

        return cart

    @staticmethod
    def get_all_products_by_cart(cart: Cart) -> list:

        all_cart_products = cart.cart_products.select_related('product')
        all_products = [cart_product.product for cart_product in all_cart_products]

        return all_products

    @staticmethod
    def is_empty_cart(cart: Cart) -> bool:

        if cart.total_products_count > 0:
            return False

        return True


class CartProductService:
    model = CartProduct

    @classmethod
    def create_cart_product(cls, cart: Cart, product: Product) -> CartProduct:

        cart_product = cls.model.objects.create(cart=cart, product=product)

        return cart_product
