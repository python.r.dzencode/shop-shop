from django.contrib import admin

from cart.models import Cart, CartProduct


@admin.register(Cart)
class CartAdmin(admin.ModelAdmin):
    list_display = ('pk', 'customer', 'total_amount', 'total_products_count', 'updated_at', 'created_at')
    list_display_links = ('customer', )
    readonly_fields = ('updated_at', 'created_at')


@admin.register(CartProduct)
class CartProductAdmin(admin.ModelAdmin):
    list_display = ('pk', 'cart', 'product', 'updated_at', 'created_at')
    list_display_links = ('cart', )
    readonly_fields = ('updated_at', 'created_at')
