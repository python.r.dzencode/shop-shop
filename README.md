<div align="center">
    <h1>
        Simple <span style="font-size: 2rem; color: #0fb8b2"> Shop-Shop </span>
    </h1>
</div>

### Possibilities:
***

#### User:
    1. New User Registration.
***

#### Product:
    1. Product creation
    2. Receiving the product.
    3. Product update
    4. Removing a product
    5. Receipt of all products.
***

#### Cart:
    1. Adding an producct to the cart.
    2. Removing a product to the cart.
    3. Removing all products from the cart.
    4. Retrieving the entire contents of the shopping cart.
***

#### Order:
    1. Possibility to place an order.
    2. Ability to delete an order.
***

## API:
    Registration:
        reg: '/api/auth/register/

    Product:
        create: /api/shop/product/create/
        read: /api/shop/product/read/<int:pk>/
        update: /api/shop/product/update/<int:pk>/
        delete: /api/shop/product/delete/<int:pk>/
        list: /api/shop/product/list/
    
    Cart:
        add product: /api/shop/cart/cart-product/add-product/
        cart contents: /api/shop/cart/volume/
        delete all: /api/shop/cart/delete-all-products-from-cart/
        delete one: /api/shop/cart/delete-one-product-from-cart/ 
            params = {"product_pk": <type:int>}

    Order:
        get order: /api/shop/order/get-order/
        delete-order: /api/shop/order/delete-order/

## API ACCESS:
    To access the api, you need an authorization token. 
    The authorization token is returned in the response after registration one-time. 
    Save the token otherwise you will no longer be able to access the registered user.
    
    The request headers must contain the Authorization key with the value "Token asdsd5d1". 
    For example: 
            {
                "Authorization": "Token asd2313fdsdfw4234dsfg234dasda"
            }

## Tests:
    count: 30
    * before starting the project, run testing.
    * must pass 30 successful tests

## Simple Docs:
    url: /api/docs/
