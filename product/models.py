from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Product(models.Model):

    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='products', null=False)
    name = models.CharField(max_length=255, blank=False)
    price = models.DecimalField(max_digits=8, decimal_places=2, blank=False)  # max number is 999999.99
    description = models.TextField(max_length=2048, blank=False)

    is_sold = models.BooleanField(default=False)

    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.name}'
