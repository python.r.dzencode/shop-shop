from django.db import transaction
from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator
from django.conf import settings

from rest_framework.viewsets import ModelViewSet
from rest_framework.authentication import TokenAuthentication, BasicAuthentication

from product.models import Product
from product.serializers import ProductCreateSerializer, ProductUpdateSerializer, ProductSerializer
from product.permissions import IsProductOwnerAndIsAuthPermissionOrReadOnly

cache_timeout = settings.DEFAULT_CACHE_TIMEOUT


@method_decorator(cache_page(cache_timeout), name='list')
@method_decorator(cache_page(cache_timeout), name='retrieve')
class ProductViewSet(ModelViewSet):
    authentication_classes = (BasicAuthentication, TokenAuthentication, )
    permission_classes = (IsProductOwnerAndIsAuthPermissionOrReadOnly, )
    queryset = Product.objects.select_related('owner')

    model = Product

    def get_serializer_class(self):
        if self.action == 'create':
            return ProductCreateSerializer

        if self.action == 'update':
            return ProductUpdateSerializer

        if self.action not in ['create', 'update']:
            return ProductSerializer

    def create(self, request, *args, **kwargs):
        with transaction.atomic():
            return super(ProductViewSet, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        with transaction.atomic():
            return super(ProductViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        with transaction.atomic():
            return super(ProductViewSet, self).destroy(request, *args, **kwargs)

