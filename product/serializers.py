from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from product.models import Product


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        exclude = ('owner', 'is_sold')
        extra_kwargs = {}

    def create(self, validated_data):
        return super(ProductSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        return super(ProductSerializer, self).update(instance, validated_data)


class ProductCreateSerializer(ProductSerializer):

    def create(self, validated_data):
        price = validated_data.get('price', None)

        if price <= 0:
            msg = {'detail': 'price < 0 or = 0. price must be > 0'}
            raise ValidationError(msg)

        request = self.context['request']
        validated_data['owner'] = request.user

        return super(ProductCreateSerializer, self).create(validated_data)


class ProductUpdateSerializer(ProductSerializer):

    class Meta(ProductSerializer.Meta):
        extra_kwargs = {
            'name': {'required': False},
            'price': {'required': False},
            'description': {'required': False},
        }

    def update(self, instance, validated_data):

        price = validated_data.get('price', None)

        if price and price <= 0:
            msg = {'detail': 'price < 0 or = 0. price must be > 0'}
            raise ValidationError(msg)

        validated_data['description'] = instance.description

        return super(ProductUpdateSerializer, self).update(instance, validated_data)
