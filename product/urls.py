from django.urls import path

from product.views import ProductViewSet

app_name = 'product'

urlpatterns = [
    path('create/', ProductViewSet.as_view({'post': 'create'}), name='create_product'),
    path('read/<int:pk>/', ProductViewSet.as_view({'get': 'retrieve'}), name='read_product'),
    path('update/<int:pk>/', ProductViewSet.as_view({'put': 'update'}), name='update_product'),
    path('delete/<int:pk>/', ProductViewSet.as_view({'delete': 'destroy'}), name='delete_product'),

    path('list/', ProductViewSet.as_view({'get': 'list'}), name='list_products'),
]
