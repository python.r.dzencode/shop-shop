from django.contrib import admin

from product.models import Product


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'owner', 'price', 'updated_at', 'created_at')
    list_display_links = ('name', )
    readonly_fields = ('updated_at', 'created_at')
