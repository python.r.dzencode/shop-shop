from django.contrib.auth import get_user_model

from product.models import Product

User = get_user_model()


class ProductService:
    model = Product

    @classmethod
    def create_new(cls, owner, name, price, description):
        new_product = cls.model.objects.create(owner=owner, name=name, price=price, description=description)

        return new_product
