from rest_framework.test import APITestCase, APIRequestFactory, force_authenticate

from auth_app.services import UserService, TokenAPIService
from product import views
from product.services import ProductService


class ProductTest(APITestCase):

    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.view = views.ProductViewSet
        self.uri = ''
        self.user = self.setup_user(username='test', password='test')
        self.user2 = self.setup_user(username='test2', password='test2')
        self.product = self.setup_product()

    @staticmethod
    def setup_user(username, password):
        user = UserService.create_new_user(username=username, password=password)
        TokenAPIService.get_or_create_token_for_user(user)
        return user

    def setup_product(self):
        new_product = ProductService.create_new(
            owner=self.user, name='Test Product',
            price=234.6, description='Test Product Description')
        return new_product

    def test_1_create_product(self):
        """
            Testing product creation with valid params.
            Test passed if response.status_code == 201.
        """
        params = {
            'name': 'Test Product Name',
            'price': 1234.56,
            'description': 'This is description for Test Product'
        }
        request = self.factory.post(path=self.uri + 'create/', data=params)
        force_authenticate(request, user=self.user)
        response = self.view.as_view({'post': 'create'})(request)
        status_code = response.status_code
        self.assertEqual(status_code, 201)

    def test_2_create_bad_price_product_2(self):
        """
            Checking price for product. Price must be > 0.
            Test passed if response.status_code == 400.
        """
        params = {
            'name': 'Test bad Product Name',
            'price': 0,
            'description': 'This is description for Test Bad Product'
        }
        request = self.factory.post(path=self.uri + 'create/', data=params)
        force_authenticate(request, user=self.user)
        response = self.view.as_view({'post': 'create'})(request)
        status_code = response.status_code
        self.assertEqual(status_code, 400)

    def test_3_create_bad_price_product_3(self):
        """
            Checking price for product. Price must be > 0.
            Test passed if response.status_code == 400.
        """
        params = {
            'name': 'Test bad Product Name',
            'price': -2,
            'description': 'This is description for Test Bad Product'
        }
        request = self.factory.post(path=self.uri + 'create/', data=params)
        force_authenticate(request, user=self.user)
        response = self.view.as_view({'post': 'create'})(request)
        status_code = response.status_code
        self.assertEqual(status_code, 400)

    def test_4_read_product(self):
        """
            Trying to get a product by pk.
            Test passed if response.status_code == 200.
        """
        url = self.uri + f'read/{self.product.pk}'
        request = self.factory.get(path=url)
        force_authenticate(request, self.user)
        response = self.view.as_view({'get': 'retrieve'})(request, pk=self.product.pk)
        status_code = response.status_code
        self.assertEqual(status_code, 200)

    def test_5_read_not_found_product(self):
        """
            Trying to get a product by pk.
            Test passed if response.status_code == 404.
        """
        url = self.uri + f'read/{99999}'
        request = self.factory.get(path=url)
        force_authenticate(request, self.user)
        response = self.view.as_view({'get': 'retrieve'})(request, pk=99999)
        status_code = response.status_code
        self.assertEqual(status_code, 404)

    def test_6_update_product(self):
        """
            Update product pk.
            Test passed if response.status_code == 200.
        """
        url = self.uri + f'update/{self.product.pk}/'
        params = {
            'name': 'Updated name for test product',
            'price': self.product.price + 1234,
            'description': 'Updated description for test product'
        }
        request = self.factory.put(path=url, data=params)
        force_authenticate(request, self.user)
        response = self.view.as_view({'put': 'update'})(request, pk=self.product.pk)
        status_code = response.status_code
        self.assertEqual(status_code, 200)

    def test_7_update_product_without_params(self):
        """
            Update product pk without params.
            Test passed if response.status_code == 200.
        """
        url = self.uri + f'update/{self.product.pk}/'
        params = {}
        request = self.factory.put(path=url, data=params)
        force_authenticate(request, self.user)
        response = self.view.as_view({'put': 'update'})(request, pk=self.product.pk)
        status_code = response.status_code
        self.assertEqual(status_code, 200)

    def test_8_update_with_wrong_price_product(self):
        """
            Update product pk with wrong price. Price is str with letter.
            Test passed if response.status_code == 400.
        """
        url = self.uri + f'update/{self.product.pk}/'
        params = {
            'name': 'Updated name for test product with wrong user',
            'price': '432123r.43',
            'description': 'Updated description for test product with wrong user'
        }
        request = self.factory.put(path=url, data=params)
        force_authenticate(request, self.user)
        response = self.view.as_view({'put': 'update'})(request, pk=self.product.pk)
        status_code = response.status_code
        self.assertEqual(status_code, 400)

    def test_9_update_with_without_access_product(self):
        """
            Update product pk with wrong price.
            Test passed if response.status_code == 403.
        """
        url = self.uri + f'update/{self.product.pk}/'
        params = {
            'name': 'Updated name for test product with wrong user',
            'price': 432123.43,
            'description': 'Updated description for test product with wrong user'
        }
        request = self.factory.put(path=url, data=params)
        force_authenticate(request, self.user2)
        response = self.view.as_view({'put': 'update'})(request, pk=self.product.pk)
        status_code = response.status_code
        self.assertEqual(status_code, 403)

    def test_10_delete_product(self):
        """
            Delete product pk.
            Test passed id response.status_code == 204.
        """
        url = self.uri + f'update/{self.product.pk}/'
        request = self.factory.delete(path=url)
        force_authenticate(request, self.user)
        response = self.view.as_view({'delete': 'destroy'})(request, pk=self.product.pk)
        status_code = response.status_code
        self.assertEqual(status_code, 204)

    def test_11_delete_wrong_pk_product(self):
        """
            Delete product pk with wrong pk.
            Test passed if response.status_code == 404.
        """
        url = self.uri + f'update/{self.product.pk + 999}/'
        request = self.factory.delete(path=url)
        force_authenticate(request, self.user)
        response = self.view.as_view({'delete': 'destroy'})(request, pk=self.product.pk + 999)
        status_code = response.status_code
        self.assertEqual(status_code, 404)

    def test_12_delete_without_access_product(self):
        """
            Delete product pk without access.
            Test passed if response.status_code == 403.
        """
        url = self.uri + f'update/{self.product.pk}/'
        request = self.factory.delete(path=url)
        force_authenticate(request, self.user2)
        response = self.view.as_view({'delete': 'destroy'})(request, pk=self.product.pk)
        status_code = response.status_code
        self.assertEqual(status_code, 403)

    def test_13_get_list_product(self):
        """
            Get all products by list.
            Test passed if response.status_code == 200.
        """
        url = self.uri + 'list/'
        request = self.factory.get(path=url)
        force_authenticate(request, self.user)
        response = self.view.as_view({'get': 'list'})(request)
        status_code = response.status_code
        self.assertEqual(status_code, 200)
